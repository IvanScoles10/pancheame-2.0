import keyMirror from 'keymirror';

module.exports = keyMirror({
    USERS_GET: null,
    USER_GET: null,
    USER_ADD: null,
    USER_UPDATE: null,
    USER_SET: null,
    USER_DELETE: null,
    USER_LOGOUT: null,
    PANCHOS_GET: null,
    LOADING_USERS: null,
    LOADING_PANCHOS: null,
    PANCHO_ADD: null,
    PANCHO_DELETE: null,
    PANCHO_CHANGE_PAYMENT: null
});
