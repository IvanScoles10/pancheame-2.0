import Events from'events';
import assign from 'object-assign';

import AppConstants from '../constants/AppConstants';
import User from '../api/User';

import AppDispatcher from '../dispatcher/AppDispatcher';
var EventEmitter = Events.EventEmitter;

var CHANGE_EVENT = 'change';

var _user = {};
var _users = {};
var _loadingUsers = true;

function addUser(user) {
    _users.push(user);
}

function setUser(user) {
    _user = user;
};

function logoutUser(user) {
    _user = user;
};

function setUsers(users) {
    _users = users;
};

function setLoadingUsers(loadingUsers) {
    _loadingUsers = loadingUsers;
}

User.getUser();
User.getUsers();

var UserStore = assign({}, EventEmitter.prototype, {

    getUser: function() {
        if (JSON.parse(window.localStorage.getItem('pancho-user')) !== null) {
            _user = JSON.parse(window.localStorage.getItem('pancho-user'));
        }

        return _user;
    },

    getUserById: function(id) {
        var user = _.find(_users, function(user) {
            return user.id === id;
        });
        return user;
    },

    getUsers: function() {
        return _users;
    },

    getLoadingUsers: function() {
        return _loadingUsers;
    },

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function(payload) {
    var action = payload.action;

    switch(action.actionType) {
        case AppConstants.USER_GET:
            setUser(payload.action.data);
        break;
        case AppConstants.USER_ADD:
            console.log('User created ' + payload.action.data);
        break;
        case AppConstants.USER_UPDATE:
            console.log('User updated ' + payload.action.data);
        break;
        case AppConstants.USER_SET:
            setUser(payload.action.data);
        break;
        case AppConstants.USER_DELETE:
            setUsers(payload.action.data);
        break;
        case AppConstants.USER_LOGOUT:
            logoutUser(payload.action.data);
        break;
        case AppConstants.USERS_GET:
            setUsers(payload.action.data);
            setLoadingUsers(false);
        break;
        default:
            return true;
        break;
    }

    UserStore.emitChange();

    return true; // No errors. Needed by promise in Dispatcher.
})

module.exports = UserStore;
