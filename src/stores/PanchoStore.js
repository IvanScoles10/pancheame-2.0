import Events from'events';
import assign from 'object-assign';

import AppConstants from '../constants/AppConstants';
import Pancho from '../api/Pancho';

import AppDispatcher from '../dispatcher/AppDispatcher';
var EventEmitter = Events.EventEmitter;

var CHANGE_EVENT = 'change';

var _pancho = {};
var _panchos = [];
var _loadingPanchos = true;

function addPancho(pancho) {
    _panchos.push(pancho);
};

function setPancho(pancho) {
    _pancho = pancho;
};

function setPanchos(panchos) {
    _panchos = panchos;
};

function setLoadingPanchos(loadingPanchos) {
    _loadingPanchos = loadingPanchos;
}

Pancho.getPanchos();

var PanchoStore = assign({}, EventEmitter.prototype, {

    getPancho: function() {
        return _pancho;
    },

    getPanchos: function() {
        return _panchos;
    },

    getLoadingPanchos: function() {
        return _loadingPanchos;
    },

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function(payload) {
    var action = payload.action;

    switch(action.actionType) {
        case AppConstants.PANCHO_ADD:
            // addPancho(payload.action.data);
            console.log('Pancho created ' + payload.action.data);
        break;
        case AppConstants.PANCHO_DELETE:
            setPanchos(payload.action.data);
            setLoadingPanchos(false);
        break;
        case AppConstants.PANCHO_CHANGE_PAYMENT:
            setPancho(payload.action.data);
            setLoadingPanchos(false);
        break;
        case AppConstants.PANCHOS_GET:
            setPanchos(payload.action.data);
            setLoadingPanchos(false);
        break;
        default:
            return true;
        break;
    }

    PanchoStore.emitChange();

    return true; // No errors. Needed by promise in Dispatcher.
})

module.exports = PanchoStore;
