import Rebase from 're-base';
var base = Rebase.createClass('https://blinding-fire-9097.firebaseio.com');

import PanchoStoreActions from '../actions/PanchoStoreActions';

module.exports = {

    getGuid() {
        return new Date().valueOf();
    },

    addPancho: function(pancho) {
        base.post('panchos/' + pancho.id, {
            context: this,
            asArray: false,
            data: pancho,
            then() {
              PanchoStoreActions.addPancho(pancho);
            }
        });
    },

    changePayment: function(pancho) {
        base.post('panchos/' + pancho.id, {
            context: this,
            asArray: false,
            data: pancho,
            then(response) {
                PanchoStoreActions.changePayment(pancho);
            }
        });
    },

    deletePancho: function(panchos, pancho) {
        base.post('panchos/' + pancho.id, {
            context: this,
            asArray: false,
            data: null,
            then() {
                var data = [];
                for (var k in panchos) {
                    if (panchos[k] !== null) {
                        if (panchos[k].id !== pancho.id) {
                            data.push(panchos[k]);
                        }
                    }
                }
                PanchoStoreActions.deletePancho(data);
            }
        });
    },

    getPanchos: function() {
        base.listenTo('panchos', {
            context: this,
            asArray: false,
            then(response) {
                var data = [];
                for (var k in response) {
                    data.push(response[k]);
                }
                PanchoStoreActions.getPanchos(data);
            }
        });
    }

};
