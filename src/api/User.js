import sha1 from 'sha1';
import Rebase from 're-base';
import Firebase from 'firebase';
import $ from 'jquery';

var FIREBASE_URL = 'https://blinding-fire-9097.firebaseio.com';
var base = Rebase.createClass(FIREBASE_URL);
var firebase = new Firebase(FIREBASE_URL + '/images');

import UserStoreActions from '../actions/UserStoreActions';

module.exports = {

    getGuid() {
        var valueOf = new Date().valueOf();
        return valueOf.toString();
    },

    getPassword() {
        return sha1('smurfs');
    },

    getUser: function() {
        var data = JSON.parse(localStorage.getItem('pancho-user') || '{}');
        UserStoreActions.getUser(data);
    },

    getUsers: function() {
        base.listenTo('users', {
            context: this,
            asArray: false,
            then(response) {
                var data = [];
                for (var k in response) {
                    data.push(response[k]);
                }
                UserStoreActions.getUsers(data);
            }
        });
    },

    addUser: function(user) {
        base.post('users/' + user.id, {
            context: this,
            asArray: false,
            data: user,
            then(data) {
                UserStoreActions.addUser(user);
            }
        });
    },

    updateUser: function(user) {
        base.post('users/' + user.id, {
            context: this,
            asArray: false,
            data: user,
            then(data) {
                UserStoreActions.updateUser(user);
            }
        });
    },

    deleteUser: function(users, user) {
        base.post('users/' + user.id, {
            context: this,
            asArray: false,
            data: null,
            then() {
                var data = [];
                for (var k in users) {
                    if (users[k] !== null) {
                        if (users[k].id !== user.id) {
                            data.push(users[k]);
                        }
                    }
                }
                UserStoreActions.deleteUser(data);
            }
        });
    },

    setUser: function(user) {
        window.localStorage.setItem('pancho-user', JSON.stringify(user));
        UserStoreActions.setUser(user);
    },

    logoutUser: function() {
        window.localStorage.clear();
        UserStoreActions.logoutUser({});
    }

};
