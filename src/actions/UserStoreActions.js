import AppDispatcher from '../dispatcher/AppDispatcher';
import AppConstants from '../constants/AppConstants';

var UserStoreActions = {

    getUser: function(data) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.USER_GET,
            data: data
        })
    },

    getUsers: function(data) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.USERS_GET,
            data: data
        })
    },

    addUser: function(data) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.USER_ADD,
            data: data
        })
    },

    updateUser: function(data) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.USER_UPDATE,
            data: data
        })
    },

    deleteUser: function(data) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.USER_DELETE,
            data: data
        })
    },

    setUser: function(data) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.USER_SET,
            data: data
        })
    },

    logoutUser: function(data) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.USER_LOGOUT,
            data: data
        })
    }

};

module.exports = UserStoreActions;
