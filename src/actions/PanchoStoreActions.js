import Rebase from 're-base';

var base = Rebase.createClass('https://blinding-fire-9097.firebaseio.com');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');

var PanchoStoreActions = {

    addPancho: function(data) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.PANCHO_ADD,
            data: data
        })
    },

    deletePancho: function(data) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.PANCHO_DELETE,
            data: data
        })
    },

    changePayment: function(data) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.PANCHO_CHANGE_PAYMENT,
            data: data
        })
    },

    getPanchos: function(data) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.PANCHOS_GET,
            data: data
        })
    }

};

module.exports = PanchoStoreActions;
