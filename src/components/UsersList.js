import React, { Component } from 'react';

import UserApi from '../api/User';

import ImageUpload from './ImageUpload';

const UserList = React.createClass({

    imageUpload(upload) {
        if (upload.file !== null && upload.user !== null) {
            upload.user.image = upload.file.base64;
            UserApi.updateUser(upload.user);
            console.log('handle uploading', upload.file);
        }
    },

    render() {
        return (
            <div>
                {this.renderItems()}
            </div>
        )
    },

    renderItems() {
        return this.props.items.map((item, index) => {
            if (item !== null) {
                return (
                    <div key={index} className="col-md-3 col-xs-3 well-container">
                        <div className="card">
                            <canvas className="header-bg" width="250" height="70" id="header-blur"></canvas>
                            <div className="avatar">
                                <ImageUpload upload={this.imageUpload} user={item} />
                            </div>
                            <div className="content">
                                <p>ID: {item.id}</p>
                                <p>Name: {item.name}</p>
                                <p>
                                    <button className="btn btn-danger" onClick={this.props.remove.bind(null, this.props.items, item)} >
                                        Eliminar
                                    </button>
                                </p>
                          </div>
                        </div>
                    </div>
                )
            }
        })
    }

});

export default UserList;
