import React from 'react';

var ImageUpload = React.createClass({

    propTypes: {
        user: React.PropTypes.object,
        upload: React.PropTypes.func
    },

    getInitialState() {
        return {
            file: null,
            imagePreviewUrl: null
        };
    },

    handleSubmit() {
        this.props.upload({
            file: this.state.file,
            user: this.props.user
        });
    },

    handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = {}
        file = e.target.files[0];
        file.base64 = null;

        var state = this;
        reader.onload = function(fileLoaded) {
            file.base64 = fileLoaded.target.result;

            state.setState({
                file: file,
                imagePreviewUrl: reader.result
            });
        }

        reader.readAsDataURL(file)
    },

    render() {
        var $imagePreview = null;

        if (this.props.user.image && this.state.imagePreviewUrl === null) {
            $imagePreview = (<img src={this.props.user.image} />);
        }

        if (this.state.imagePreviewUrl !== null) {
            $imagePreview = (<img src={this.state.imagePreviewUrl} />);
        }

        return (
            <div className="previewComponent">
                {($imagePreview == null) ? <img src={'/images/default.gif'} />: $imagePreview}
                <form className="image-upload" action="" onSubmit={this.handleSubmit}>
                    <span className="btn btn-default btn-file">
                        Select Image <input type="file" onChange={this.handleImageChange} />
                    </span>
                    <button className="btn btn-success" type="submit">Upload Image</button>
                </form>
            </div>
        )
    }
});

export default ImageUpload;
