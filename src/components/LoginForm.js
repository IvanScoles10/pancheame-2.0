import React from 'react';

const LoginForm = React.createClass({

    getInitialState() {
        return {
            isEmptyForm: true
        };
    },

    handleSubmit(event) {
        event.preventDefault();

        this.props.login({
            email: this.refs.email.value,
            password: this.refs.password.value
        });

        this.refs.email.value = '';
        this.refs.password.value = '';
    },

    handleChange(event) {
        event.preventDefault();

        var empty = true;
        if (
            this.refs.email.value !== '' &&
            this.refs.password.value !== ''
        ) {
            empty = false;
        }
        this.setState({isEmptyForm: empty});
    },

    renderInput(type, ref, placeholder) {
        return (
            <div className="form-group">
                <label for={ref}>{placeholder}</label>
                <input {...this.getInputProps(type, ref, placeholder)} />
            </div>
        )
    },

    renderSubmitButton() {
        return (
            <button
                type="submit"
                className="btn btn-primary pull-right"
                disabled={this.state.isEmptyForm}>Submit</button>
        )
    },

    renderLoginInvalid() {
        return (
            <div className="alert alert-danger" role="alert">Invalid email or password</div>
        )
    },

    render() {
        return (
            <form {...this.getFormProps()}>
                <div className="col-md-12">
                    <div className="form-login">
                        {this.renderInput('text', 'email', 'Email')}
                        {this.renderInput('password', 'password', 'Password')}
                        {(this.props.loginInvalid === true) ? this.renderLoginInvalid() : ''}
                        {this.renderSubmitButton()}
                    </div>
                </div>
            </form>
        )
    },

    getFormProps() {
        return {
            action: '',
            autoComplete: 'off',
            onSubmit: this.handleSubmit
        }
    },

    getInputProps(type, ref, placeholder) {
        return {
            className: 'form-control',
            type: type,
            id: ref,
            name: ref,
            ref: ref,
            placeholder: placeholder,
            onChange: this.handleChange
        }
    }

})

export default LoginForm;
