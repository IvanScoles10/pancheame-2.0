import React from 'react';

import { Link } from 'react-router'
import _ from 'lodash';

import AppHeader from './AppHeader';

const App = React.createClass({
    render() {
        return(
            <div className="app-container">
                <AppHeader/>
                <div className="container">
                    {this.props.children}
                </div>
            </div>
        )
    }
});

export default App;
