import React from 'react';
import FacebookLogin from 'react-facebook-login';
import _ from 'lodash';
import sha1 from 'sha1';

import LoginForm from './LoginForm';

import UserStore from '../stores/UserStore';
import UserApi from '../api/User';

import Panchos from './Panchos';
import PanchosList from './PanchosList';

function getLoginState() {
    return {
        loginInvalid: false,
        loadingUsers: UserStore.getLoadingUsers(),
        user: UserStore.getUser(),
        users: UserStore.getUsers()
    }
}

const Login = React.createClass({

    getInitialState() {
        return getLoginState();
    },

    onChange() {
        this.setState(getLoginState());
    },

    handleLogin(item) {
        var user = _.find(this.state.users, function(user) {
            return user.email === item.email && user.password === sha1(item.password);
        });

        if (user) {
            console.log('Existing user ' + user);
            UserApi.setUser(user);
        } else {
            this.setState({
                loginInvalid: true,
                loadingUsers: UserStore.getLoadingUsers(),
                user: UserStore.getUser(),
                users: UserStore.getUsers()
            });
        }
    },

    componentDidMount() {
        UserStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        UserStore.removeChangeListener(this.onChange);
    },

    responseFacebook(response) {
        if (typeof response.accessToken !== 'undefined') {
            var user = _.find(this.state.users, function(item) {
                return item.id === response.id;
            });

            if (!user) {
                console.log('New user ' + response.name);

                UserApi.addUser(response);
                UserApi.setUser(response);
            } else {
                console.log('Existing user ' + response.name);
                UserApi.setUser(response);
            }
        } else {
            console.log('User Undefined');
        }
    },

    getButtonProps() {
        return {
            appId: '1023173104427430',
            autoLoad: true,
            xfbml: true,
            version: '2.3',
            language: 'en_US',
            scope: 'public_profile,email',
            callback: this.responseFacebook
        };
    },

    isLogged: function() {
        return JSON.stringify(this.state.user) !== JSON.stringify({});
    },

    render() {
        return (<div className="router-container">
            <div className="container-login">
                {this.state.loadingUsers === true && this.state.user ? <h3>LOADING...</h3> : this.renderLogin()}
                {(this.isLogged() === true) ? this.renderPanchosList() : ''}
            </div>
        </div>)
    },

    renderLogin() {
        return (<div>
            <LoginForm login={this.handleLogin} loginInvalid={this.state.loginInvalid} />
        </div>)
    },

    renderPanchosList() {
        return (<div className="container-panchos" >
            {this.state.loadingUsers === true && this.state.user ? <h3>LOADING...</h3> : <PanchosList {...this.getButtonProps()} />}
        </div>)
    }

})

export default Login;
