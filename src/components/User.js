import React, { Component } from 'react';
import Rebase from 're-base';

import UserStore from '../stores/UserStore';

import UserApi from '../api/User';
import UserForm from './UserForm';
import UsersList from './UsersList';

function getUserState() {
    return {
        loadingUsers: UserStore.getLoadingUsers(),
        user: UserStore.getUser(),
        users: UserStore.getUsers()
    }
}

const User = React.createClass({

    getInitialState() {
        return getUserState();
    },

    onChange() {
        this.setState(getUserState());
    },

    componentDidMount() {
        UserStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        UserStore.removeChangeListener(this.onChange);
    },

    handleAddItem(item) {
        item.id = UserApi.getGuid();
        item.password = UserApi.getPassword();
        UserApi.addUser(item);
    },

    handleRemoveItem(users, user) {
        UserApi.deleteUser(users, user);
    },

    render() {
        return (
            <div className="main-container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="form-user">
                            <h3 className="text-center">Usuarios</h3>
                            <UserForm add={this.handleAddItem} />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                    {this.state.loadingUsers === true ? <h3 className="text-center"> LOADING... </h3> : <UsersList items={this.state.users} remove={this.handleRemoveItem} />}
                    </div>
                </div>
            </div>
        )
    }

})

export default User;
