import React from 'react';

import UserStore from '../stores/UserStore';

import Login from './Login';
import PanchosList from './PanchosList';

function getPanchosState() {
    return {
        user: UserStore.getUser()
    }
}

const Panchos = React.createClass({

    getInitialState() {
        return getPanchosState();
    },

    onChange() {
        this.setState(getPanchosState());
    },

    componentDidMount() {
        UserStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        UserStore.removeChangeListener(this.onChange);
    },

    isLogged() {
        return JSON.stringify(this.state.user) !== JSON.stringify({});
    },

    render() {
        return(
            <div className="main-container">
                {(this.isLogged() === false) ? <Login /> : ''}
                {(this.isLogged() === true) ? <PanchosList /> : ''}
            </div>
        )
    }

});

export default Panchos;
