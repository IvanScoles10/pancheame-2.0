import React from 'react';
import { Link, hashHistory } from 'react-router'
import _ from 'lodash';

import UserStore from '../stores/UserStore';
import UserApi from '../api/User';

function getAppHeaderState() {
    return {
        user: UserStore.getUser()
    }
}

const AppHeader = React.createClass({
    getInitialState() {
        return getAppHeaderState();
    },

    onChange() {
        this.setState(getAppHeaderState());
    },

    componentDidMount() {
        UserStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        UserStore.removeChangeListener(this.onChange);
    },

    getLinkProps(url) {
        return {
            activeClassName: 'active',
            className: (this.isLogged()) ? '' : ' hide',
            to: url
        };
    },

    getLogoutProps() {
        return {
            activeClassName: 'active',
            className: (this.isLogged()) ? '' : ' hide',
            onClick: this.handleClick
        };
    },

    isLogged() {
        return JSON.stringify(this.state.user) !== JSON.stringify({});
    },

    handleClick(event) {
        event.preventDefault();
        UserApi.logoutUser();

        hashHistory.push('/');
    },
    render() {
        return(
            <nav className="navbar navbar-default navbar-fixed-top">
                <div className="container">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                      </button>
                      <Link to="/" className="navbar-brand">Pancha App</Link>
                    </div>
                    <div id="navbar" className="navbar-collapse collapse">
                        <ul className="nav navbar-nav">
                            <li><Link {...this.getLinkProps('/')}>Home</Link></li>
                            <li><Link {...this.getLinkProps('/users')}>Usuarios</Link></li>
                            <li><a {...this.getLogoutProps()}>Logout</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }

});

export default AppHeader;
