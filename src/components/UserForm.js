import React, { Component } from 'react';

const UserForm = React.createClass({

    getInitialState() {
        return {
            isEmptyForm: true
        };
    },

    handleSubmit(event) {
        event.preventDefault();

        this.props.add({
            email: this.refs.email.value,
            name: this.refs.name.value
        });

        this.refs.email.value = '';
        this.refs.name.value = '';
    },

    handleChange(event) {
        event.preventDefault();

        var empty = true;
        if (
            this.refs.email.value !== '' &&
            this.refs.name.value !== ''
        ) {
            empty = false;
        }
        this.setState({isEmptyForm: empty});
    },

    renderInput(ref, placeholder) {
        return (
            <div className="form-group">
                <label for={ref}>{placeholder}</label>
                <input {...this.getInputProps(ref, placeholder)} />
            </div>
        )
    },

    renderSubmitButton() {
        return (
            <button
                type="submit"
                className="btn btn-primary pull-right"
                disabled={this.state.isEmptyForm}>Submit</button>
        )
    },

    render() {
        return (
            <form {...this.getFormProps()}>
                <div className="col-md-12">
                    {this.renderInput('email', 'Email')}
                    {this.renderInput('name', 'Name')}
                    {this.renderSubmitButton()}
                </div>
            </form>
        )
    },

    getFormProps() {
        return {
            action: '',
            autoComplete: 'off',
            onSubmit: this.handleSubmit
        }
    },

    getInputProps(ref, placeholder) {
        return {
            className: 'form-control',
            type: 'text',
            id: ref,
            name: ref,
            ref: ref,
            placeholder: placeholder,
            onChange: this.handleChange
        }
    }

})

export default UserForm;
