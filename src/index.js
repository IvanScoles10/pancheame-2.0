import React from 'react';
import { render } from 'react-dom';

import { Router, Route, IndexRoute, useRouterHistory, hashHistory } from 'react-router'

import App from './components/App';
import Panchos from './components/Panchos';
import User from './components/User.js';

import './assets/stylesheets/base.scss';
import 'bootstrap/dist/css/bootstrap.css';

render(
    <Router history={hashHistory}>
        <Route path="/" name="app" component={App}>
            <IndexRoute name="panchos" component={Panchos} />
            <Route path="/users" name="users" component={User} />
        </Route>
    </Router>, document.getElementById('app')
);
